import { Component } from '@angular/core';
import { AuthService } from './core-services/index';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'my-app1';

  constructor(private authService: AuthService) {
    console.log(this.authService.test());
  }
}
