import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthService } from './index';

@NgModule({
  declarations: [],
  imports: [CommonModule]
})
export class CoreServicesModule {

  static forRoot() {
    return {
      ngModule: CoreServicesModule,
      providers: [AuthService]
    };
  }
}
